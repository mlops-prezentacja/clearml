from clearml import Task
from clearml.automation.controller import PipelineController


# Connecting ClearML with the current process,
# from here on everything is logged automatically
task = Task.init(project_name='MLOPS', task_name='Pipeline',
                 task_type=Task.TaskTypes.controller, reuse_last_task_id=False)

pipe = PipelineController(default_execution_queue='default', add_pipeline_tags=False)
pipe.add_step(name='stage_preprocess', base_task_project='MLOPS', base_task_name='Preprocess')
pipe.add_step(
    name='stage_train', 
    parents=['stage_preprocess', ],
    base_task_project='MLOPS', base_task_name='train',
)
pipe.add_step(
    name='stage_eval', 
    parents=['stage_train', ],
    base_task_project='MLOPS',
    base_task_name='Evaluation',
)

# Starting the pipeline (in the background)
pipe.start()
# Wait until pipeline terminates
pipe.wait()
# cleanup everything
pipe.stop()

print('done')
