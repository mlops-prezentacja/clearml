import argparse
import pandas as pd
import numpy as np
from clearml import Task, Dataset

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_id", type=str, required=True, help="ID of the dataset")
    # parser.add_argument("--out_train", type=str, required=True, help="Column with classname")
    # parser.add_argument("--out_test", type=str, required=True, help="Column with classname")
    args = parser.parse_args()

    task = Task.init(project_name="MLOPS", task_name="Preprocess")

    dataset_file = Dataset.get(dataset_id=args.dataset_id).get_local_copy()
    print(dataset_file)

    df = pd.read_csv(dataset_file + "/iris.csv")
    train, test = np.split(df.sample(frac=1), [int(.6*len(df))])

    # train.to_csv(args.out_train, index=False)
    # test.to_csv(args.out_test, index=False)
    task.upload_artifact('train', train)
    task.upload_artifact('test', train)