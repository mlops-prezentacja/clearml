import argparse
import pandas as pd
from sklearn.metrics import f1_score, accuracy_score
import json
from clearml import Task, Dataset

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # parser.add_argument("--predictions", type=str, required=True, help="Path to train dataset")
    parser.add_argument("--train_task_id", type=str, required=True, help="Train task id")
    parser.add_argument("--preprocess_task_id", type=str, required=True, help="Original dataset id")
    parser.add_argument("--target_column", type=str, required=True, help="Column with classname")
    # parser.add_argument("--result_file", type=str, required=True, help="Path to result file (in json)")
    args = parser.parse_args()

    task = Task.init(project_name="MLOPS", task_name="Evaluation")

    train_task = Task.get_task(task_id=args.train_task_id)
    preprocess_task = Task.get_task(task_id=args.preprocess_task_id)
    predctions_file = train_task.artifacts['predictions'].get_local_copy()
    dataset_file = preprocess_task.artifacts['test'].get_local_copy()
    
    # Load train dataset
    predictions = pd.read_csv(predctions_file)[args.target_column]
    trues = pd.read_csv(dataset_file)[args.target_column]

    f1 = f1_score(trues, predictions, average='macro')
    accuracy = accuracy_score(trues, predictions)

    #with open(args.result_file, 'w') as f:
    #    json.dump({
    #        'accuracy': accuracy,
    #        'f1': f1
    #    }, f, indent=4)
    task.upload_artifact("metrics", {
        'accuracy': accuracy,
        'f1': f1
    })
