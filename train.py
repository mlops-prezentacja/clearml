import argparse
import pandas as pd
from sklearn import tree
import pickle
from clearml import Task
import pickle
import joblib

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # parser.add_argument("--train_dataset", type=str, required=True, help="Path to train dataset")
    # parser.add_argument("--test_dataset", type=str, required=True, help="Path to train dataset")
    parser.add_argument("--preprocess_task_id", type=str, required=True, help="ID of the previous task")
    parser.add_argument("--target_column", type=str, required=True, help="Column with classname")
    # parser.add_argument("--model_path", type=str, required=True, help="Path where the model will be saved")
    # parser.add_argument("--predictions_path", type=str, required=True, help="Path where model predictions will be saved")
    parser.add_argument("--max_depth", type=int, default=1, help="Max tree depth")
    args = parser.parse_args()

    task = Task.init(project_name="MLOPS", task_name="train")
    preprocess_task = Task.get_task(task_id=args.preprocess_task_id)
    df_train_pickle = preprocess_task.artifacts['train'].get_local_copy()
    df_test_pickle = preprocess_task.artifacts['test'].get_local_copy()

    print(df_train_pickle)

    # Load train dataset
    # df_train = pd.read_csv(args.train_dataset)
    df_train = pd.read_csv(df_train_pickle)
    y_train = df_train[args.target_column]
    xs_train = df_train.drop(args.target_column, axis=1)

    # Load test dataset
    # df_test = pd.read_csv(args.test_dataset)
    df_test = pd.read_csv(df_train_pickle)
    xs_test = df_test.drop(args.target_column, axis=1)

    # Train
    clf = tree.DecisionTreeClassifier(max_depth=args.max_depth)
    clf.fit(xs_train, y_train)

    # Save model
    # with open(args.model_path, 'wb') as f:
    #    pickle.dump(clf, f)
    # (potencjalnie) task.upload_artifact("model", clf)
    joblib.dump(clf, 'clf.pkl', compress=True)


    # Save predictions
    predictions = pd.DataFrame()
    predictions[args.target_column] = clf.predict(xs_test)
    # predictions.to_csv(args.predictions_path, index=False)
    task.upload_artifact("predictions", predictions)